import re
import langid
import pandas as pd
from textblob import TextBlob
from snorkel.labeling import labeling_function, PandasLFApplier, LFAnalysis
from snorkel.labeling.model import LabelModel
from snorkel.preprocess import preprocessor
from profanity_filter import ProfanityFilter
from sklearn.model_selection import train_test_split

CLEANUP = 0
SEED = 123

NEGATIVE = 0
NEUTRAL = 1
POSITIVE = 2
ABSTAIN = -1

pf = ProfanityFilter()


@preprocessor(memoize=True)
def textblob_sentiment(x):
    scores = TextBlob(x.clean)
    x.polarity = scores.sentiment.polarity
    x.subjectivity = scores.sentiment.subjectivity
    return x


@labeling_function(pre=[textblob_sentiment])
def textblob_polarity(x):
    if x.polarity > 0.05:
        return POSITIVE
    elif x.polarity < -0.05:
        return NEGATIVE
    elif x.polarity == 0:
        return NEUTRAL
    else:
        return ABSTAIN


@labeling_function(pre=[textblob_sentiment])
def textblob_subjectivity(x):
    return NEUTRAL if x.subjectivity <= 0.05 else ABSTAIN


@labeling_function()
def lf_profanity(x):
    return NEGATIVE if pf.is_profane(x.clean) else ABSTAIN


@labeling_function()
def lf_keyword_negative(x):
    return NEGATIVE if any(word in re.split('\W+', x.clean.lower()) for word in
                           ['kill', 'kills', 'sucks', 'ineffective', 'expensive', 'bad', 'worse',
                            'worst', 'awful', 'unacceptable', 'garbage', 'inferior', 'defective', 'harmful',
                            'dangerous', 'unhealthy', 'damaging']) else ABSTAIN


@labeling_function()
def lf_keyword_positive(x):
    return POSITIVE if any(word in re.split('\W+', x.clean.lower()) for word in
                           ['effective', 'inexpensive', 'awesome', 'amazing', 'great', 'good', 'better',
                            'best', 'nice', 'superior', 'acceptable', 'useful', 'excellent', 'wonderful', 'superb',
                            'efficient', 'trustworthy', 'helpful', 'convenient', 'perfect', 'solid', 'safe',
                            'reliable']) else ABSTAIN


def read_file(file):
    n_dataset = pd.read_csv(file)
    n_dataset = n_dataset[['tweet']]
    return n_dataset['tweet'].values.tolist()


def cleanup(tweets):
    clean = []
    for tweet in tweets:
        tweet = re.sub(r'@[A-Za-z0-9_]+', '', tweet)
        tweet = re.sub(r'#', '', tweet)
        tweet = re.sub(r'RT : ', '', tweet)
        tweet = re.sub(r'http\S+|www\S+|https\S+', '', tweet)
        clean.append(tweet)
    return clean


def get_lang(tweets):
    langs = []
    for tweet in tweets:
        lang = langid.classify(str(tweet))[0]
        langs.append(lang)
    return langs


if __name__ == '__main__':
    if CLEANUP:
        # Load data
        data = pd.DataFrame({'tweet': read_file('file.csv')})
        # data = data[:1000]

        # Clean data
        print("Cleaning data...")
        data['clean'] = cleanup(data['tweet'])

        # Remove short tweets
        print("Removing short tweets...")
        data = data.loc[data['clean'].str.len() >= 3]

        # Remove non-english tweets
        print("Removing non-english tweets...")
        data['lang'] = get_lang(data['clean'])
        data = data.loc[data['lang'] == 'en']
        data.to_csv('english.csv')
    else:
        # Load data
        data = pd.DataFrame(pd.read_csv('english.csv'))

    # Label data
    print("Labelling data...")
    lfs = [textblob_polarity,
           textblob_subjectivity,
           lf_profanity,
           lf_keyword_negative,
           lf_keyword_positive]
    L_data = PandasLFApplier(lfs).apply(data)
    print(LFAnalysis(L_data, lfs).lf_summary())
    label_model = LabelModel(cardinality=3, verbose=True)
    label_model.fit(L_train=L_data, n_epochs=500, log_freq=100, seed=SEED)
    data['class'] = label_model.predict(L=L_data)

    # Remove abstain data
    data = data.loc[data['class'] != -1]
    data['class'] = data['class'] - 1

    # Save labelled data to file
    data.to_csv('labelled.csv')

    # Split data to train and test
    [train, test] = train_test_split(data, test_size=0.2, random_state=SEED)
    train.to_csv('train.csv')
    test.to_csv('test.csv')

    print('Done')
