import pandas as pd
from textblob import TextBlob
import string
import textstat
import os
import matplotlib.pyplot as plt
from wordcloud import WordCloud
from textwrap3 import wrap


# get polarity of tweets
def get_polarity(tweets):
    polarity = []
    for tweet in tweets:
        testimonial = TextBlob(str(tweet))
        polarity.append(testimonial.sentiment.polarity)
    return polarity

# exclude words for wordcloud generation
def remove_words(tweets):
    copyTweets = []
    for tweet in tweets:
        tweet = tweet.lower()
        tweet = tweet.replace('-',' ')
        splitted_tweet = tweet.split(' ')
        while any(word == 'covid' or word == 'covid19' or word == 'vaccine' or word == 'covidvaccine' or word == 'will' for word in splitted_tweet) :
            for word in splitted_tweet:
                if word == 'covid' or word == 'covid19' or word == 'vaccine' or word == 'covidvaccine' or word == 'will':
                    splitted_tweet.remove(word)
                    break
        copyTweets.append(" ".join(splitted_tweet))
    return copyTweets

# word cloud generation
def generate_wordcloud(tweets, title, name_of_file, willRemoveWords):
    joined_tweets = ""
    if willRemoveWords:
        copyTweets = remove_words(tweets)
        joined_tweets = " ".join(copyTweets)
    else :
        joined_tweets = " ".join(tweets)
    wc = WordCloud(width = 400, height=330, max_words = 150, colormap="Dark2").generate(joined_tweets)
    plt.figure(figsize=(10,8))
    plt.imshow(wc, interpolation='bilinear')
    plt.axis("off")
    plt.title('\n'.join(wrap(title,60)), fontsize=13)
    plt.savefig(name_of_file)
    plt.clf()

    
# load filtered_data
filtered_data = pd.DataFrame(pd.read_csv('labelled.csv'))
en_sentiments = get_polarity(filtered_data['clean'])

if not os.path.exists('EDA'):
    os.makedirs('EDA')

positive_tweets = filtered_data[filtered_data['class'] > 0]
neutral_tweets = filtered_data[filtered_data['class'] == 0]
negative_tweets = filtered_data[filtered_data['class'] < 0]

generate_wordcloud(positive_tweets['clean'], "Positive labelled tweets", 'EDA/positive_tweets_wordcloud.png', True)
generate_wordcloud(neutral_tweets['clean'], "Neutral labelled tweets", 'EDA/neutral_tweets_wordcloud.png', True)
generate_wordcloud(negative_tweets['clean'], "Negative labelled tweets", 'EDA/negative_tweets_wordcloud.png', True)


# histogram of polarity
plt.hist(en_sentiments)
plt.savefig('EDA/polarity_histogram.png')
plt.clf()

polarityDF = pd.DataFrame(en_sentiments, columns=['polarity'])
polarityDF['tweets'] = filtered_data['clean']

# 3 lowest polarity tweets
polarityDF = polarityDF.sort_values(by=['polarity'])
print("3 tweets with lowest polarity: ", end="\n")
print(polarityDF.iloc[:3], end="\n")

# 3 highest polarity tweets
polarityDF = polarityDF.sort_values(by=['polarity'], ascending=False, na_position='first')
print("3 tweets with highest polarity: ", end="\n")
print(polarityDF.iloc[:3], end="\n")

# store sorted tweets by polarity in csv file
polarityDF.to_csv('EDA/sorted_polarity_df.csv')


### ATTENTION: KEEP COMMENTED
# readability text_standard
#text_standard = filtered_data['clean'].apply(lambda x : textstat.text_standard(x))
#print ('Text standard of tweets = ', text_standard.mode(), end="\n")
