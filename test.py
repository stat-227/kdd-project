import re
import os
import pandas as pd
import seaborn as sns
from joblib import load
from textwrap3 import wrap
from textblob import TextBlob
from wordcloud import WordCloud
import matplotlib.pyplot as plt
from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer
from sklearn.pipeline import Pipeline
from sklearn.metrics import classification_report, accuracy_score, plot_confusion_matrix


#MODEL = 'naive-bayes'
MODEL = 'random-forest'


## for word cloud generation

def remove_words(tweets):
    copyTweets = []
    for tweet in tweets:
        tweet = tweet.lower()
        tweet = tweet.replace('-',' ')
        splitted_tweet = tweet.split(' ')
        while any(word == 'covid' or word == 'covid19' or word == 'vaccine' or word == 'covidvaccine' or word == 'will' for word in splitted_tweet) :
            for word in splitted_tweet:
                if word == 'covid' or word == 'covid19' or word == 'vaccine' or word == 'covidvaccine' or word == 'will':
                    splitted_tweet.remove(word)
                    break
        copyTweets.append(" ".join(splitted_tweet))
    return copyTweets

def generate_wordcloud(tweets, title, name_of_file, willRemoveWords):
    joined_tweets = ""
    if willRemoveWords:
        copyTweets = remove_words(tweets)
        joined_tweets = " ".join(copyTweets)
    else :
        joined_tweets = " ".join(tweets)
    wc = WordCloud(width = 400, height=330, max_words = 150, colormap="Dark2").generate(joined_tweets)
    plt.figure(figsize=(10,8))
    plt.imshow(wc, interpolation='bilinear')
    plt.axis("off")
    plt.title('\n'.join(wrap(title,60)), fontsize=13)
    plt.savefig(name_of_file)
    plt.clf()

## FEATURE ENGINEERING

def preprocess(tweet):
    tweet.lower()

    # Remove urls
    tweet = re.sub(r"http\S+|www\S+|https\S+", '', tweet, flags=re.MULTILINE)

    #Generating the list of words in the tweet (hastags and other punctuations removed)
    def form_sentence(tweet):
        tweet_blob = TextBlob(tweet)
        return ' '.join(tweet_blob.words)

    new_tweet = form_sentence(tweet)
    
    #Removing stopwords and words with unusual symbols
    def no_user_alpha(tweet):
        tweet_list = [ele for ele in tweet.split() if ele != 'user']
        clean_tokens = [t for t in tweet_list if re.match(r'[^\W\d]*$', t)]
        clean_s = ' '.join(clean_tokens)
        clean_mess = [word for word in clean_s.split() if word.lower() not in stopwords.words('english')]
        return clean_mess

    no_punc_tweet = no_user_alpha(new_tweet)
    
    #Normalizing the words in tweets
    def normalization(tweet_list):
        lem = WordNetLemmatizer()
        normalized_tweet = []
        for word in tweet_list:
            normalized_text = lem.lemmatize(word,'v')
            normalized_tweet.append(normalized_text)
        return normalized_tweet
    
    return normalization(no_punc_tweet)

pipeline = load(MODEL + '.joblib')
if not os.path.exists(MODEL):
    os.makedirs(MODEL)

test = pd.DataFrame(pd.read_csv('test.csv'))

## MODEL EVALUATION

predictions = pipeline.predict(test['clean'])
print(accuracy_score(predictions, test['class']))

# Output classification report to csv
cfReport = classification_report(predictions, test['class'], output_dict=True)
cfDF = pd.DataFrame(cfReport).transpose()
cfDF.to_csv(MODEL + '/classification_report.csv')

# Output confusion matrix
plot_confusion_matrix(pipeline, test['clean'], test['class'])
plt.savefig(MODEL + '/confusionMatrix_test.png')
plt.clf()

# Output frequency plot for vaccine sentiments
predictionsDF = pd.DataFrame(predictions, columns=['predictions'])
sns.countplot(x='predictions', data=predictionsDF)
plt.savefig(MODEL + '/vaccine_sentiments.png')
plt.clf()

# Output frequency plot for sentiments on vaccine developers
predictionsDF['tweet_test'] = test['clean']
predictionsDF['tweet_list'] = predictionsDF['tweet_test'].apply(preprocess)

aztrazeneca = []
pfizer = []
sinovac = []
moderna = []
    
def generate_bar_graph(tweets_sentiments, name_of_file, title):
    sentimentsDF = pd.DataFrame(tweets_sentiments, columns=['sentiments'])
    groupedSentiments = sentimentsDF['sentiments'].value_counts()
    vaccineDF = pd.DataFrame(groupedSentiments.index.tolist(), columns=['sentiments'])
    vaccineDF['count'] = groupedSentiments.tolist()
    pct = []
    for x in vaccineDF['count']:
        pct.append(x / sum(vaccineDF['count']) * 100)
    vaccineDF['percentage'] = pct
    sns.barplot(x='sentiments', y='percentage', data=vaccineDF).set_title(title)
    plt.savefig(MODEL + name_of_file)
    plt.clf()


for i in range(predictionsDF.shape[0]):
    for word in predictionsDF.iloc[i, 2]:
        if word.lower() == "aztrazeneca" or word.lower() == "aztra" or word.lower() == "zeneca" or word.lower == "oxford" or word.lower == "aztrazenecaoxford":
            aztrazeneca.append(predictionsDF.iloc[i,0])
            continue
        elif word.lower() == "pfizer" or word.lower() == "biontech" or word.lower() == "pfizerbiontech":
            pfizer.append(predictionsDF.iloc[i,0])
            continue
        elif word.lower() == "sinovac":
            sinovac.append(predictionsDF.iloc[i,0])
            continue
        elif word.lower() == "moderna":
            moderna.append(predictionsDF.iloc[i,0])
            continue
        else: continue
    
if len(aztrazeneca) != 0:
    generate_bar_graph(aztrazeneca, '/aztrazenica_sentiments.png', 'Sentiments for AztraZenica')

if len(pfizer) != 0:
    generate_bar_graph(pfizer, '/pfizer_sentiments.png', 'Sentiments for Pfizer-BioNTech')

if len(moderna) != 0:
    generate_bar_graph(moderna, '/moderna_sentiments.png', 'Sentiments for Moderna')

if len(sinovac) != 0:
    generate_bar_graph(sinovac, '/sinovac_sentiments.png', 'Sentiments for Sinovac')

# Word Cloud - determine general comments about covid-19 vaccine

positive_pred = predictionsDF[predictionsDF['predictions'] > 0]
neutral_pred = predictionsDF[predictionsDF['predictions'] == 0]
negative_pred = predictionsDF[predictionsDF['predictions'] < 0]

generate_wordcloud(positive_pred['tweet_test'], "Positive sentiments", MODEL + '/positive_sentiments_wordcloud.png', True)

generate_wordcloud(neutral_pred['tweet_test'], "Neutral sentiments", MODEL + '/neutral_sentiments_wordcloud.png', True)

generate_wordcloud(negative_pred['tweet_test'], "Negative sentiments", MODEL + '/negative_sentiments_wordcloud.png', True)


print('Done')
