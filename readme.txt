GENERAL INSTRUCTIONS:
1. pip install -r requirements.txt
2. run main.py - to generate clean up date. Resulting files: labelled.csv
3. run EDA.py - to generate EDA graphs. Input files: labelled.csv. Resulting files: graphs/wordloud of EDA in EDA/ folder.
4. run train.py - to generate random forest model. Input files: labelled.csv. Resulting files: train.csv, test.csv, and random-forest.joblib
5. run test.py - to check results of trained model. Input files: random-forest.joblib and test.csv. Resulting files: graphs/tables/matrix/wordcloud of results in random-forest folder.
